package com.modanisa.acceptancetest;

import com.thoughtworks.gauge.Step;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class StepImplementation extends BaseTest {

    @Step("<key> saniye kadar bekle")
    public void waitWithSecond(int key) throws InterruptedException {
        Thread.sleep(key * 1000);
    }

    @Step("<element> elementine tıkla.")
    public void clickElementByCss(String element) {
        driver.findElement(By.cssSelector(element)).click();
    }

    @Step("<element> elementini bul ve <key> değerini yaz.")
    public void sendKeyByCss(String element, String key) {
        driver.findElement(By.cssSelector(element)).sendKeys(key);
    }

    @Step("<element> elementini bul ve <key> değerine eşitliğini kontrol et")
    public void checkInput(String element, String key) {
        List<WebElement> elementList = driver.findElements(By.cssSelector(element));
        WebElement lastElement = elementList.get(elementList.size() - 1);
        String expected = key;
        Assertions.assertThat(lastElement.getText()).isEqualTo(expected);
    }

    @Step("Go to todo page")
    public void implementation1() {
        driver.get(BASE_URL);
    }
}

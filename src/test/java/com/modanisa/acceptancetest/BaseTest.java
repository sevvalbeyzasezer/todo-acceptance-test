package com.modanisa.acceptancetest;

import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    public static WebDriver driver;
    public static final String BASE_URL = "http://localhost:8080/";

    @BeforeScenario
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(10,TimeUnit.SECONDS);
    }

    @AfterScenario
    public void tearDown() {
        driver.quit();
    }
}
